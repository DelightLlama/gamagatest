﻿using UnityEngine;

[CreateAssetMenu(fileName = "HazardData", menuName = "Items/Hazard", order = 62)]
public class HazardData : ItemData {

    [SerializeField]
    private int _damagePoints;

    public int damagePoints {
        get {
            return _damagePoints;
        }
    }

}
