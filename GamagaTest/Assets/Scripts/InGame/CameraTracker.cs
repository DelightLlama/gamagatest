﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTracker : MonoBehaviour {

	[SerializeField]
	[Tooltip("The greater the number, less smooth the tracking will be.")]
	private float _trackRigidness;

	[SerializeField]
	private Transform _gameObjectToTrack;

	private float _xOffset = 0, _yOffset = 0, _rigidnessOffset = 0;

	private Vector3 _pos, _offset;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (_gameObjectToTrack != null) {
			_offset = _gameObjectToTrack.position;
			_offset.x += _xOffset;
			transform.GetChild(0).LookAt(_offset);
			_offset.y += _yOffset;
			_pos = Vector3.Lerp(transform.position, _offset, (_trackRigidness + _rigidnessOffset) * Time.deltaTime);
			_pos.z = transform.position.z;
			transform.position = _pos;
			//Debug.Log(_trackRigidness + " + " + _rigidnessOffset + " = " + (_trackRigidness + _rigidnessOffset) + " | + deltaTime: " + ((_trackRigidness + _rigidnessOffset) + Time.deltaTime));
		}
	}

	public void SetObjectToTrack(Transform objectToTrack) {
		_gameObjectToTrack = objectToTrack;
	}

	//To modify the tracking rigidness without losing the initial setting, just to add to the base value
	public void SetTrackingRigidnessOffset(float rigidOffset) {
		_rigidnessOffset = rigidOffset;
		if (_rigidnessOffset < 0) {
			_rigidnessOffset = 0;
		}
	}

	public float xOffset { get { return _xOffset; } set { _xOffset = value; } }

	public float yOffset { get { return _yOffset; } set { _yOffset = value; } }

	public bool tracking { get { return (_gameObjectToTrack != null); } }

}
