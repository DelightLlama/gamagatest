﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GTGameScene : GTSceneBase {

	[SerializeField]
	private Player _player;

	[SerializeField]
	private CylinderTerrain _terrain;

	[SerializeField]
	private GameObject[] _collectablesTypes;

	[SerializeField]
	private int _spawnNumberOfEachCollectable;

	[SerializeField]
	private float _collectableSpawnRate = 3f;

	[SerializeField]
    private GameObject[] _enemyTypes;

	[SerializeField]
	private int _spawnNumberOfEachEnemy;

	[SerializeField]
	private float _enemySpawnRate = 3f;

	[SerializeField]
	private CameraTracker _cameraTracker;

	[SerializeField]
	private GameObject _canvas;

	[SerializeField]
	private GameObject _healthBar;

	[SerializeField]
	private ScoreScreen _scoreScreen;

	[SerializeField]
	private AudioClip _collectAudioClip;

	private int _score, _friesCount, _iceCreamsCount, _hamburgersCount, _collectableCountLimit = 9999;
	private TextMeshProUGUI _friesHUDText, _iceCreamsHUDText, _hamburgersHUDText;
	private IEnumerator _crHealth, _crCollectableSpawn, _crEnemySpawn, _crDifficulty;

	private List<Collectable> _collectablePool;

	private List<Enemy> _enemyPool;

	protected Globals.GAME_STATES _state;

	private bool _mouseDown = false;

	private KeyCode _key = KeyCode.None;

	// Use this for initialization
	public override void Start () {
        base.Start();
		StartCoroutine(SetReady());
	}
	
	// Update is called once per frame
	public override void Update () {
		switch (_state) {
			case Globals.GAME_STATES.INITIALIZING:
				/*
				if ((_player.gameObject.activeSelf) && (_player.avatar != null) && (_cameraTracker.tracking)) {
					PlayerStatus();
				}
				*/
				break;
			case Globals.GAME_STATES.GAME:
				if (Input.GetMouseButtonDown(0)) {//Is the left mouse click been pressed
					_mouseDown = true;
					_key = KeyCode.None;
					if (Input.mousePosition.y >= (Screen.height / 2)) {
						RotatePlayer(-45f);
					} else {
						RotatePlayer(-135f);
					}
				} else if (Input.anyKeyDown && !_mouseDown) {
					if (_key == KeyCode.None) {
						if (Input.GetKey(KeyCode.W) && (Input.GetKey(KeyCode.S))) {//If the player holds both keys no rotation, being -90 the neutral degree;
							_key = KeyCode.None;
							RotatePlayer(-90f);
						} else if (Input.GetKey(KeyCode.W)) {//If the player holds W key, player's avatar rotates -45 degrees;
							_key = KeyCode.W;
							RotatePlayer(-45f);
						} else if (Input.GetKey(KeyCode.S)) {//If the player holds W key, player's avatar rotates -135 degrees;
							_key = KeyCode.S;
							RotatePlayer(-135f);
						}
					}
				}

				if (_mouseDown) {
					if (Input.GetMouseButtonUp(0)) {
						RotatePlayer(-90f);
						_mouseDown = false;
					}
				} else if (_key != KeyCode.None) {
					if (Input.GetKeyUp(_key)) {
						RotatePlayer(-90f);
						_key = KeyCode.None;
					}
				}
				PlayerStatus();
				break;
		}
		
		base.Update();
	}

	//What collectable is collected, and it's score is added in the total points variable "_score";
	private void CollectableCollision (string[] collectableType) {
		PlayAudioClip(_collectAudioClip);
		switch (collectableType[0]) {
			case "Fry":
				_friesCount++;
				if (_friesCount >= _collectableCountLimit) {
					_friesCount = _collectableCountLimit;
				}
				break;
			case "IceCream":
				_iceCreamsCount++;
				if (_iceCreamsCount >= _collectableCountLimit) {
					_iceCreamsCount = _collectableCountLimit;
				}
				break;
			case "Hamburger":
				_hamburgersCount++;
				if (_hamburgersCount >= _collectableCountLimit) {
					_hamburgersCount = _collectableCountLimit;
				}
				break;
		}
		_score += int.Parse(collectableType[1]);
		UpdateCollectablesCollected();
	}

	//the player's avatar is given a target degrees value for the rotation in the x axis
	private void RotatePlayer (float degrees) {
		//Debug.Log(degrees);
		_player.xAxisRotationTarget = degrees;
	}

	//We get the player's avatar current status to modify the cylinderTerrain and the camera's tracking
	private void PlayerStatus() {
		//the difference between the current x rotation and the neutral degrees
		float delta = (_player.playerXRotation + 90f);
		//percentage of the delta and the rotation limit, the player x axis rotation limits to 45 degrees
		delta = delta / 45f;
		if (_player.avatar.transform.position.y > _terrain.transform.position.y) {//if the player is rotating up
			delta *= -1f;
			_terrain.AddXAxisRotation(delta);
		} else if (_player.avatar.transform.position.y < _terrain.transform.position.y) {//if the player is rotating down
			_terrain.AddXAxisRotation(delta);
		} else {//if the player is no rotating
			_terrain.AddXAxisRotation(0);
		}

		//The camera tracking offset
		_cameraTracker.xOffset = 5f;
		_cameraTracker.yOffset = (delta * -1);
	}

	private void Play() {
		//the player can start updating he position and rotation
		_player.Play();
		//The game loop state is set to game
		_state = Globals.GAME_STATES.GAME;

		if (_crDifficulty == null) {
			_crDifficulty = DifficultyIncrease();
			StartCoroutine(_crDifficulty);
		}

		if (_crHealth == null) {
			_crHealth = Health();
			StartCoroutine(_crHealth);
		}
	}

	//Ienumerator to make a GameOver sequence
    protected virtual IEnumerator GameOver() {
		//The game loop state is set to END
		_state = Globals.GAME_STATES.OVER;

		DeactivateAllCollectables();

		iTween.MoveTo(_healthBar.transform.parent.gameObject, iTween.Hash("position", new Vector3(380f, 500f, 0), "time", 1f, "isLocal", true));

		iTween.MoveTo(_friesHUDText.transform.parent.gameObject,      iTween.Hash("position", new Vector3(-580f, 500f, 0), "time", 1f, "isLocal", true));
		iTween.MoveTo(_iceCreamsHUDText.transform.parent.gameObject,  iTween.Hash("position", new Vector3(-370f, 500f, 0), "time", 1f, "isLocal", true));
		iTween.MoveTo(_hamburgersHUDText.transform.parent.gameObject, iTween.Hash("position", new Vector3(-160f, 500f, 0), "time", 1f, "isLocal", true));

		yield return StartCoroutine(_scoreScreen.Animate(_friesCount, _iceCreamsCount, _hamburgersCount, _score, _player.healthPoints));
    }

	//When the Retry button is pressed
	private void ResetGame() {
		_scoreScreen.Hide();
		StartCoroutine(SetReady());
	}

	private void ReloadGame() {
        Globals.LoadScene(Scenes.Game);
    }

	private void QuitGame() {
		Application.Quit();
	}

	private IEnumerator CountDownToGame() {
		//Countdown to start the game
		float tMinus = 3f;
		Color[] countDownColors = new Color[4] {
			new Color(0, 1f, 0),
			new Color(240f/255f, 1f, 0),
			new Color(1f, 150f/255f, 0),
			new Color(1f, 0, 0)
		};
		TextMeshProUGUI countDownText = _canvas.transform.Find("CountDownText").GetComponent<TextMeshProUGUI>();
		do {
			countDownText.text = tMinus.ToString();
			countDownText.color = countDownColors[(int)tMinus];
			yield return new WaitForSeconds(1f);
			tMinus -= 1f;
		} while (tMinus > 0);
		countDownText.text = "GO!";
		countDownText.color = countDownColors[(int)tMinus];
		yield return new WaitForSeconds(0.5f);
		countDownText.text = "";
		Play();
	}

	private void DeactivateAllCollectables() {
		foreach (Collectable c in _collectablePool) {
			c.Deactivate();
			c.transform.position = _terrain.transform.Find("Start").position;
		}
	}

	private void DeactivateAllEnemies() {
		foreach (Enemy e in _enemyPool) {
			e.Deactivate();
			e.transform.position = _terrain.transform.Find("Start").position;
		}
	}

	private IEnumerator Health() {
		do {
			if (_state.Equals(Globals.GAME_STATES.GAME)) {
				_healthBar.transform.localScale = new Vector3((float)_player.healthPoints / (float)_player.baseHealthPoints, 1f, 1f);
			}
			yield return null;
		} while (true);
	}

	private IEnumerator CollectableSpawning() {
		do {
			if (_state.Equals(Globals.GAME_STATES.GAME)) {
				int index;
				do {
					yield return null;
					index = Random.Range(0, _collectablePool.Count);
				} while (!_collectablePool[index].state.Equals("inactive"));
				_collectablePool[index].Activate();
			}
			yield return new WaitForSeconds(_collectableSpawnRate);
		} while (true);
	}

	private IEnumerator EnemySpawning() {
		do {
			if (_state.Equals(Globals.GAME_STATES.GAME)) {
				int index;
				do {
					yield return null;
					index = Random.Range(0, _enemyPool.Count);
				} while (!_enemyPool[index].state.Equals("passive"));
				_enemyPool[index].Activate();
			}
			yield return new WaitForSeconds(_enemySpawnRate);
		} while (true);
	}

	private IEnumerator DifficultyIncrease() {
		float noHutTime = 0, difficultyUPTime = 3f, offset = 0.02f;
		do {
			if ((_state.Equals(Globals.GAME_STATES.GAME)) && (_player.playerState.Equals(Player.PLAYER_STATES.PLAY))) {
				noHutTime += Time.deltaTime;
				if (noHutTime >= difficultyUPTime) {
					noHutTime = 0;
					offset += offset;
				}
			} else {
				noHutTime = 0;
				offset = 0.02f;
			}
			_cameraTracker.SetTrackingRigidnessOffset(offset);
			yield return null;
		} while (true);
	}

	private void UpdateCollectablesCollected() {
		_friesHUDText.text      = _friesCount.ToString();
		_iceCreamsHUDText.text  = _iceCreamsCount.ToString();
		_hamburgersHUDText.text = _hamburgersCount.ToString();
	}

	private IEnumerator SetReady() {
		int i, j;

		_state = Globals.GAME_STATES.INITIALIZING;

		if (!_terrain.gameObject.activeSelf) {
			_terrain.gameObject.SetActive(true);
			//Three frames to let the terrain to set.
			yield return null; yield return null; yield return null;
		}
		_terrain.transform.rotation = new Quaternion();
		yield return null; yield return null; yield return null;

		if (!_player.gameObject.activeSelf) {
			_player.gameObject.SetActive(true);
			//Three frames to let the player to set.
			yield return null; yield return null; yield return null;
		}

		_player.transform.position = Vector3.zero;
		yield return null; yield return null; yield return null;
		//Waiting for the player's Avatar to be instantiated
		while (_player.avatar == null) {
			yield return null;
		}

		_player.Set();
		yield return null; yield return null; yield return null;

		//The player will move towards the given gameobject
		_player.SetGoal(_terrain.transform.Find("Finish").position);

		//Setting the player's avatar "above" the terrain, and "on front" of the camera, by getting the radius of the terrain + the player padding property as y coordenate, and rotating the avatar's holder 270 degress in the x axis to face the camera
		float radius = (_terrain.transform.Find("Start").GetComponent<MeshRenderer>().bounds.extents.z + _player.terrainPadding);
		_player.SetAvatarLocalPosition(new Vector3(0, radius, 0));
		yield return null; yield return null; yield return null;
		_player.SetAvatarLocalRotation(new Vector3(-90f, 0, 0));
		yield return null; yield return null; yield return null;

		//Setting player status change callbacks
		_player.targetCallback = gameObject;//This gameObject is the target
		_player.onGoalReachedCallback = "GameOver";
		_player.onDeadCallback = "GameOver";

		//Setting collision callbacks, collectable and enemies
		_player.SetCollisionTargetCallback(gameObject);//This gameObject is the target
		_player.SetCollectableCollisionCallback("CollectableCollision");//The method name that will be called when a collectable collides with the player's avatar

		//The camera is set to follow the player's avatar
		_cameraTracker.SetObjectToTrack(_player.avatar.transform);

		if (_collectablePool == null) {
			_collectablePool = new List<Collectable>();
		}
		if (_collectablePool.Count < _spawnNumberOfEachCollectable) {
			for (i = 0; i < _collectablesTypes.Length; i++) {
				for (j = 0; j < _spawnNumberOfEachCollectable; j++) {
					_collectablePool.Add(Instantiate(_collectablesTypes[i], _terrain.transform).GetComponent<Collectable>());
					_collectablePool[_collectablePool.Count - 1].name = _collectablePool[_collectablePool.Count - 1].GetComponent<Collectable>().CollectableType;
					_collectablePool[_collectablePool.Count - 1].gameObject.transform.localScale = new Vector3(_collectablePool[_collectablePool.Count - 1].gameObject.transform.localScale.x / _terrain.transform.localScale.x,
																											   _collectablePool[_collectablePool.Count - 1].gameObject.transform.localScale.y / _terrain.transform.localScale.y,
																											   _collectablePool[_collectablePool.Count - 1].gameObject.transform.localScale.z / _terrain.transform.localScale.z);
					yield return null; yield return null; yield return null;
					_collectablePool[_collectablePool.Count - 1].SetSpriteLocalPosition(new Vector3(0, radius, 0));
				}
			}
		}

		//All collectables are removed from view, sending them to the beginig of the terrain
		DeactivateAllCollectables();
		yield return null; yield return null; yield return null;

		//A collectable spawner coroutine is set, continously spawnig collectables, the collectable class search for the player and spawns on front of it
		if (_crCollectableSpawn == null) {
			_crCollectableSpawn = CollectableSpawning();
			StartCoroutine(_crCollectableSpawn);
		}

		if (_enemyPool == null) {
			_enemyPool = new List<Enemy>();
		}
		if (_enemyPool.Count < _spawnNumberOfEachEnemy) {
			for (i = 0; i < _enemyTypes.Length; i++) {
				for (j = 0; j < _spawnNumberOfEachEnemy; j++) {
					_enemyPool.Add(Instantiate(_enemyTypes[i], _terrain.transform).GetComponent<Enemy>());
					_enemyPool[_enemyPool.Count - 1].name = _enemyPool[_enemyPool.Count - 1].GetComponent<Enemy>().type;
					_enemyPool[_enemyPool.Count - 1].gameObject.transform.localScale = new Vector3(_enemyPool[_enemyPool.Count - 1].gameObject.transform.localScale.x / _terrain.transform.localScale.x,
																								   _enemyPool[_enemyPool.Count - 1].gameObject.transform.localScale.y / _terrain.transform.localScale.y,
																								   _enemyPool[_enemyPool.Count - 1].gameObject.transform.localScale.z / _terrain.transform.localScale.z);
					yield return null; yield return null; yield return null;
					_enemyPool[_enemyPool.Count - 1].SetAvatarLocalPosition(new Vector3(0, radius, 0));
				}
			}
		}

		//A enemy spawner coroutine is set, continously spawnig enemies, the enemy class search for the player and spawns on front of it
		if (_crEnemySpawn == null) {
			_crEnemySpawn = EnemySpawning();
			StartCoroutine(_crEnemySpawn);
		}

		//All enemies are removed from view, sending them to the beginig of the terrain
		DeactivateAllEnemies();
		yield return null; yield return null; yield return null;

		//Waiting for the canvas gameObject reference to not be null
		while (_canvas == null) {
			yield return null;
		}

		while ((_canvas.transform.Find("Fries/Text").GetComponent<TextMeshProUGUI>() == null) || 
			   (_canvas.transform.Find("IceCreams/Text").GetComponent<TextMeshProUGUI>() == null) || 
			   (_canvas.transform.Find("Hamburgers/Text").GetComponent<TextMeshProUGUI>() == null)) {
			yield return null;
		}

		_friesHUDText      = _canvas.transform.Find("Fries/Text").GetComponent<TextMeshProUGUI>();
		_iceCreamsHUDText  = _canvas.transform.Find("IceCreams/Text").GetComponent<TextMeshProUGUI>();
		_hamburgersHUDText = _canvas.transform.Find("Hamburgers/Text").GetComponent<TextMeshProUGUI>();

		_score = 0;
		_friesCount      = 0;
		_iceCreamsCount  = 0;
		_hamburgersCount = 0;

		UpdateCollectablesCollected();

		_healthBar.transform.localScale = Vector3.one;
		iTween.MoveTo(_healthBar.transform.parent.gameObject, iTween.Hash("position", new Vector3(380f, 300f, 0), "time", 1f, "isLocal", true));

		iTween.MoveTo(_friesHUDText.transform.parent.gameObject,      iTween.Hash("position", new Vector3(-580f, 300f, 0), "time", 1f,   "isLocal", true));
		iTween.MoveTo(_iceCreamsHUDText.transform.parent.gameObject,  iTween.Hash("position", new Vector3(-370f, 300f, 0), "time", 1.2f, "isLocal", true));
		iTween.MoveTo(_hamburgersHUDText.transform.parent.gameObject, iTween.Hash("position", new Vector3(-160f, 300f, 0), "time", 1.4f, "isLocal", true));

		_scoreScreen.targetGameObject = gameObject;
		_scoreScreen.onRetryCallback = "ResetGame";
		_scoreScreen.onQuitCallback = "QuitGame";

		StartCoroutine(CountDownToGame());
	}

}
