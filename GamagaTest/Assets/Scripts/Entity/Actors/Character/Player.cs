﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character {

	[SerializeField]
	private int _healthPoints;

	[SerializeField]
	private AudioClip _hurtAudioClip;

	protected float _xAxisRotationTarget = 0;

	private PLAYER_STATES _playerState;

	private IEnumerator _cRoutineHurt;

	public string onGoalReachedCallback, onDeadCallback;

	public GameObject targetCallback;

	// Use this for initialization
	public override void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		switch (_playerState) {
			case PLAYER_STATES.IDLE:

				break;
			case PLAYER_STATES.PLAY:
				Step();
				break;
			case PLAYER_STATES.HURT:
				Step();
				break;
			case PLAYER_STATES.DEAD:

				break;
		}
	}

	private void Step() {
		if (Vector3.Distance(transform.position, _goal) > 0.1f) {
			//transform.position = Vector3.Lerp(transform.position, _goal.position, _horizontalSpeed * Time.deltaTime);
			transform.position = Vector3.MoveTowards(transform.position, _goal, _horizontalSpeed * Time.deltaTime);
		} else {
			GoalReached();
		}
		//transform.position = new Vector3(transform.position.x + _horizontalSpeed, transform.position.y, transform.position.z);
		_avatar.transform.parent.rotation = Quaternion.Slerp(_avatar.transform.parent.rotation, Quaternion.Euler(_xAxisRotationTarget, 0, 0), _AxisRotationSmooth * Time.deltaTime);
	}

	public override void Set() {
		_playerState = PLAYER_STATES.IDLE;
		_healthPoints = (_data != null) ? (_data as PlayerData).healthPoints : 0;
		_horizontalSpeed = (_data != null) ? _data.horizontalSpeed : 0;
		_xAxisRotationTarget = -90f;
		transform.position = Vector3.zero;
		if (_avatar != null) {
			_avatar.transform.parent.rotation = new Quaternion();
		}
		ResetAvatar();
		//_playerGO.transform.parent.Rotate(new Vector3(-90f, 0, 0));
	}

	public void Play() {
		_horizontalSpeed = _data.horizontalSpeed;
		_playerState = PLAYER_STATES.PLAY;
	}

	public void GoalReached() {
		_playerState = PLAYER_STATES.IDLE;
		if (_cRoutineHurt != null) {
			StopCoroutine(_cRoutineHurt);
		}
		ResetAvatar();
		if ((targetCallback != null) && (Utils.StringNotEmpty(onGoalReachedCallback))) {
			targetCallback.SendMessage(onGoalReachedCallback);
		}
	}

	public void Damage(int damagePoint) {
		if (!_playerState.Equals(PLAYER_STATES.PLAY)) {
			return;
		}
		_healthPoints -= damagePoint;
		DamageAvatarFlash();
		if (_healthPoints <= 0) {
			Dead();
		} else {
			_playerState = PLAYER_STATES.HURT;
			_cRoutineHurt = HurtSequence();
			StartCoroutine(_cRoutineHurt);
		}
	}

	private void Dead() {
		_playerState = PLAYER_STATES.DEAD;
		if (_cRoutineHurt != null) {
			StopCoroutine(_cRoutineHurt);
		}
		if ((targetCallback != null) && (Utils.StringNotEmpty(onDeadCallback))) {
			targetCallback.SendMessage(onDeadCallback);
		}
	}

	private IEnumerator HurtSequence() {
		float hurtTime = 3f, flashTime = 0;
		do {
			hurtTime -= Time.deltaTime;
			flashTime += Time.deltaTime;
			if (flashTime >= 0.1f) {
				_avatar.GetComponent<MeshRenderer>().enabled = !_avatar.GetComponent<MeshRenderer>().enabled;
				flashTime = 0;
			}
			yield return null;
		} while (hurtTime > 0);
		ResetAvatar();
		_playerState = PLAYER_STATES.PLAY;
		_cRoutineHurt = null;
	}

	private void DamageAvatarFlash() {
		_avatar.GetComponent<MeshRenderer>().enabled = false;
		_avatar.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;
		Globals.PlayClip(_hurtAudioClip);
	}

	private void ResetAvatar() {
		if (_avatar == null) {
			return;
		}
		_avatar.GetComponent<MeshRenderer>().enabled = true;
		_avatar.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.black;
	}

	protected override IEnumerator SetReady() {
		yield return StartCoroutine(base.SetReady());

		while (_avatar.GetComponent<PlayerCollisionDetector>() == null) {
			yield return null;
		}

		_avatar.GetComponent<PlayerCollisionDetector>().onEnemyTriggerTarget = gameObject;
		_avatar.GetComponent<PlayerCollisionDetector>().onEnemyCollisionCallback = "Damage";
	}

	public void SetGoal(Vector3 goal) {
		_goal = goal;
	}

	public void SetCollisionTargetCallback (GameObject targetCallback) {
		_avatar.GetComponent<PlayerCollisionDetector>().onItemTriggerTarget = targetCallback;
	}

	public void SetCollectableCollisionCallback(string callbackName) {
		_avatar.GetComponent<PlayerCollisionDetector>().onItemCollisionCallback = callbackName;
	}

	public PLAYER_STATES playerState { get { return _playerState; } }

	public int healthPoints { get { return _healthPoints; } }

	public int baseHealthPoints { get { return (_data as PlayerData).healthPoints; } }

	public float xAxisRotationTarget { get { return _xAxisRotationTarget; } set { _xAxisRotationTarget = value; } }
	
	public float terrainPadding { get { return _data.terrainPadding; } }

	public float playerXRotation { get { return _avatar.transform.parent.eulerAngles.x - 360f; } }

	public enum PLAYER_STATES {
		IDLE,
		PLAY,
		HURT,
		DEAD,
	}

}
