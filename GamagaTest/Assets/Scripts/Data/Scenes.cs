﻿using UnityEngine;
using System.Collections;

public class Scenes {
    public const string LoadingScene = "_Scenes/LoadingScene";
    public const string Menu =         "_Scenes/MenuScene";
    public const string Game =         "_Scenes/GameScene";
}
