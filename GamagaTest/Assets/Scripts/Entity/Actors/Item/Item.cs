﻿using UnityEngine;

public class Item : Actor {

	[SerializeField]
	protected int _points;

	[SerializeField]
	[Tooltip("At what distance on front the player should appear when Activate() is called.")]
	protected float _spawnDistance = 10f;

	protected Vector3 _locaPos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
	}

	public int points { get { return _points; } }

}
