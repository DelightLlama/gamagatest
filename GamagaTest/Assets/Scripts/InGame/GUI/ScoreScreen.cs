﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScreen : MonoBehaviour {

	private GameObject retryBttn, quitBttn;

	public string onRetryCallback, onQuitCallback;

	public GameObject targetGameObject;

	// Use this for initialization
	void Start () {
		retryBttn = transform.Find("RetryButton").gameObject;
		quitBttn = transform.Find("QuitButton").gameObject;

		retryBttn.SetActive(false);
		quitBttn.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Hide() {
		transform.localPosition = new Vector3(1500f, transform.position.y, transform.position.z);
	}

	public IEnumerator Animate(int totalFries, int totalIceCreams, int totalHamburgers, int finalScore, int healthPoints) {
		retryBttn.SetActive(false);
		quitBttn.SetActive(false);

		if (healthPoints > 0) {
			transform.Find("goodBG").GetComponent<Image>().enabled = true;
			transform.Find("badBG").GetComponent<Image>().enabled = false;
		} else {
			transform.Find("goodBG").GetComponent<Image>().enabled = false;
			transform.Find("badBG").GetComponent<Image>().enabled = true;
		}

		transform.Find("TotalFries/Text").GetComponent<TextMeshProUGUI>().text = totalFries.ToString();
		transform.Find("TotalIceCreams/Text").GetComponent<TextMeshProUGUI>().text = totalIceCreams.ToString();
		transform.Find("TotalHamburgers/Text").GetComponent<TextMeshProUGUI>().text = totalHamburgers.ToString();

		transform.Find("FinalScore").GetComponent<TextMeshProUGUI>().text = "= 0 Pts";

		iTween.MoveTo(gameObject, iTween.Hash("position", Vector3.zero, "time", 1f, "isLocal", true));

		yield return new WaitForSeconds(1.2f);

		iTween.ValueTo(gameObject, iTween.Hash("from", totalFries, "to", 0, "time", 1f, "onUpdateTarget", gameObject, "onUpdate", "OnUpdateFries"));
		iTween.ValueTo(gameObject, iTween.Hash("from", totalIceCreams, "to", 0, "time", 1f, "onUpdateTarget", gameObject, "onUpdate", "OnUpdateIceCreams", "delay", 1f));
		iTween.ValueTo(gameObject, iTween.Hash("from", totalHamburgers, "to", 0, "time", 1f, "onUpdateTarget", gameObject, "onUpdate", "OnUpdateHamburgers", "delay", 2f));

		iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", finalScore, "time", 3f, "onUpdateTarget", gameObject, "onUpdate", "OnUpdateFinalScore"));

		yield return new WaitForSeconds(3.2f);

		retryBttn.SetActive(true);
		quitBttn.SetActive(true);
	}

	private void OnUpdateFries(int delta) {
		transform.Find("TotalFries/Text").GetComponent<TextMeshProUGUI>().text = delta.ToString();
	}

	private void OnUpdateIceCreams(int delta) {
		transform.Find("TotalIceCreams/Text").GetComponent<TextMeshProUGUI>().text = delta.ToString();
	}

	private void OnUpdateHamburgers(int delta) {
		transform.Find("TotalHamburgers/Text").GetComponent<TextMeshProUGUI>().text = delta.ToString();
	}

	private void OnUpdateFinalScore(int delta) {
		transform.Find("FinalScore").GetComponent<TextMeshProUGUI>().text = "= " + delta.ToString() + " Pts";
	}

	public void OnRetryClick() {
		if ((targetGameObject != null) && (Utils.StringNotEmpty(onRetryCallback))) {
			targetGameObject.SendMessage(onRetryCallback);
		}
	}

	public void OnQuitClick() {
		if ((targetGameObject != null) && (Utils.StringNotEmpty(onQuitCallback))) {
			targetGameObject.SendMessage(onQuitCallback);
		}
	}

}
