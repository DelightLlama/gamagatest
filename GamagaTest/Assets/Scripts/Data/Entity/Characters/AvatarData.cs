﻿using UnityEngine;

public class AvatarData : CharacterData {

	[SerializeField]
	private float _xAxisRotationSpeed = 0;

	[SerializeField]
	private GameObject _gameObject;

	public GameObject gameObject { get { return _gameObject; } }

	public float xAxisRotationSpeed { get { return _xAxisRotationSpeed; } }

}
