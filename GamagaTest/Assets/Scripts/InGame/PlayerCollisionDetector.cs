﻿using UnityEngine;

public class PlayerCollisionDetector : MonoBehaviour {

	public string onItemCollisionCallback, onEnemyCollisionCallback;

	public GameObject onItemTriggerTarget, onEnemyTriggerTarget;

	public void OnTriggerEnter(Collider collision) {
		if (collision.gameObject.tag.Equals("Collectable")) {
			collision.gameObject.transform.parent.GetComponent<Collectable>().Deactivate();
			if ((onItemTriggerTarget != null) && (Utils.StringNotEmpty(onItemCollisionCallback))) {
				string[] args = new string[2] { collision.gameObject.transform.parent.GetComponent<Collectable>().CollectableType, collision.gameObject.transform.parent.GetComponent<Collectable>().points.ToString() };
				onItemTriggerTarget.SendMessage(onItemCollisionCallback, args);
			}
		} else if (collision.gameObject.tag.Equals("Enemy")) {
			collision.gameObject.transform.parent.parent.GetComponent<Enemy>().Deactivate();
			if ((onEnemyTriggerTarget != null) && (Utils.StringNotEmpty(onEnemyCollisionCallback))) {
				onEnemyTriggerTarget.SendMessage(onEnemyCollisionCallback, collision.gameObject.transform.parent.parent.GetComponent<Enemy>().damage);
			}
		} else {
			Debug.LogWarning("WTF did I collide with!!!!?: " + collision.gameObject.name);
		}
	}

}
