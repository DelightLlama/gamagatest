﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectUtility : MonoBehaviour {

    //public float _wantedAspectRatio = Globals.screenWidth / Globals.screenHeight;
    static float wantedAspectRatio;
    static Camera myCam;
    static Camera bgCam;

    void Awake() {
        myCam = GetComponent<Camera>();
        if (!myCam) {
            myCam = Camera.main;
        }
        if (!myCam) {
            Debug.Log("No camera available!");
            return;
        }
        Screen.SetResolution(Globals.screenWidth, Globals.screenHeight, false);
        myCam.aspect = (float)Globals.screenWidth / Globals.screenHeight;
        myCam.orthographicSize = Globals.screenSize;
        //wantedAspectRatio = _wantedAspectRatio;
        wantedAspectRatio = (float)Globals.screenWidth / Globals.screenHeight;
        SetCamera();
    }

    public static void SetCamera() {
        float currentAspectRatio = (float)Screen.width / Screen.height;
        //If the current aspect ratio is already approximately equal to the desired aspect ratio,
        //use a full-screen Rect (in case it was set to something else previously)
        if (((int)(currentAspectRatio * 100) / 100.0f) == ((int)(wantedAspectRatio * 100) / 100.0f)) {
            myCam.rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
            if (bgCam) {
                Destroy(bgCam.gameObject);
            }
            return;
        }
        //if: Pillarbox / else: Letterbox
        if (currentAspectRatio > wantedAspectRatio) {
            float inset = 1.0f - (wantedAspectRatio / currentAspectRatio);
            myCam.rect = new Rect(inset / 2, 0.0f, 1.0f - inset, 1.0f);
        } else {
            float inset = 1.0f - (currentAspectRatio / wantedAspectRatio);
            myCam.rect = new Rect(0.0f, inset/2, 1.0f, 1.0f - inset);
        }
        //myCam.pixelRect = new Rect(0, 0, Globals.originalScreenWidth, Globals.originalScreenHeight);
        if (!bgCam) {
            //Make a new camera behind the normal camera which displays black; otherwise the unused space is undefined
            bgCam = new GameObject("BackgroundCamera", typeof(Camera)).GetComponent<Camera>();
            bgCam.depth = int.MinValue;
            bgCam.clearFlags = CameraClearFlags.SolidColor;
            bgCam.backgroundColor = Color.black;
            bgCam.cullingMask = 0;
        }
    }

    public static int screenWidth {
        get {
            return (int)(Screen.width * myCam.rect.width);
        }
    }

    public static int screenHeight {
        get {
            return (int)(Screen.height * myCam.rect.height);
        }
    }

    public static int xOffset {
        get {
            return (int)(Screen.width * myCam.rect.x);
        }
    }

    public static int yOffset {
        get {
            return (int)(Screen.height * myCam.rect.y);
        }
    }

    public static Rect screenRect {
        get {
            return new Rect(myCam.rect.x * Screen.width, myCam.rect.y * Screen.height, myCam.rect.width * Screen.width, myCam.rect.height * Screen.height);
        }
    }

    public static Vector3 mousePosition {
        get {
            Vector3 mousePos = Input.mousePosition;
            mousePos.x -= (int)(myCam.rect.x * Screen.width);
            mousePos.y -= (int)(myCam.rect.y * Screen.height);
            return mousePos;
        }
    }

    public static Vector2 guiMousePosition {
        get {
            Vector2 mousePos = Event.current.mousePosition;
            mousePos.x = Mathf.Clamp(mousePos.x, myCam.rect.x * Screen.width, (myCam.rect.x * Screen.width) + (myCam.rect.width * Screen.width));
            mousePos.y = Mathf.Clamp(mousePos.y, myCam.rect.y * Screen.height, (myCam.rect.y * Screen.height) + (myCam.rect.height * Screen.height));
            return mousePos;
        }
    }

}
