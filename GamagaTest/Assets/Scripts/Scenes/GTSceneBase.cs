﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GTSceneBase : MonoBehaviour {

	// Use this for initialization
	public virtual void Start () {
#if UNITY_ANDROID
		Input.multiTouchEnabled = false;
#endif
		Globals.audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}

    private void OnApplicationFocus(bool focus) {
        if (focus) {
            Globals.Resume();
        } else {
            Globals.Pause();
        }
    }

    private void OnApplicationPause(bool pause) {
        if (pause) {
            Globals.Pause();
        } else {
            Globals.Resume();
        }
    }

    protected float PlayAudioClip(AudioClip clip, bool stopOtherAudios = false, float volume = 1f) {
        return Globals.PlayClip(clip, stopOtherAudios, volume);
    }

}
