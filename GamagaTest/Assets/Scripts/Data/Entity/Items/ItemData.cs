﻿using UnityEngine;

public class ItemData : EntityData {

    [SerializeField]
    private Sprite _sprite;

    public Sprite sprite {
        get {
            return _sprite;
        }
    }

}
