﻿using UnityEngine;

public class EntityData : ScriptableObject {

    [SerializeField]
    private string _entityName;
    [SerializeField]
    private float _horizontalSpeed = 0;

    public string entityName { get { return _entityName; } }

    public float horizontalSpeed { get { return _horizontalSpeed; } }

}
