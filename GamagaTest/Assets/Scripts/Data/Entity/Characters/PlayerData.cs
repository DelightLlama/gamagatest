﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Characters/Player", order = 51)]
public class PlayerData : AvatarData {

    [SerializeField]
    private int _healthPoints = 1000;

    public int healthPoints { get { return _healthPoints; } }

}
