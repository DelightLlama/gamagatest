﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor {

	private void OnEnable() {
		
	}

	public override void OnInspectorGUI() {
		Player script = (Player)target;
		DrawDefaultInspector();
		if (GUILayout.Button("Set Default Data values")) {
			if (script != null) {
				script.Set();
			}
		}
	}

}
