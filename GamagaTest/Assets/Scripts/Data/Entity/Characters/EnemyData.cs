﻿using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "Characters/Enemy", order = 52)]
public class EnemyData : AvatarData {

	[SerializeField]
    private bool _estationary = false;
    [SerializeField]
    private int _damagePoints = 1;

	public bool estationary { get { return _estationary; } }

    public int damagePoints { get { return _damagePoints; } }

}
