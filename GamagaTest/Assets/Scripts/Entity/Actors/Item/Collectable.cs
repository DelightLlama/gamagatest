﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : Item {

	[SerializeField]
	protected CollectableData _data;

	// Use this for initialization
	void Start () {
		StartCoroutine(SetReady());
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
		switch (_state) {
			case "idle":

				break;
			case "active":
				//If the collectable is out side the camera
				if ((GameObject.FindGameObjectWithTag("Player").transform.position.x - 100f) >= transform.position.x) {
					Deactivate();
				}
				break;
			case "inactive":

				break;
		}
	}

	public void Set() {
		_state = "idle";
		_points = (_data != null) ? _data.points : 0;
		_horizontalSpeed = (_data != null) ? _data.horizontalSpeed : 0;
		_xAxisRotationSpeed = 0f;
		transform.position = Vector3.zero;
		Deactivate();
	}

	public override void Activate() {
		transform.position = GameObject.FindGameObjectWithTag("Player").transform.position + (new Vector3(_spawnDistance, 0, 0));
		transform.Find("SpriteHolder").localPosition = _locaPos;
		transform.Rotate(Random.Range(0, 360f), 0, 0);
		//transform.Rotate(270f, 0, 0);
		_state = "active";
	}

	public override void Deactivate() {
		_state = "inactive";
		transform.Find("SpriteHolder").localPosition = Vector3.zero;
		transform.rotation = new Quaternion(0, 0, 0, 0);
	}

	private IEnumerator SetReady() {
		transform.Find("SpriteHolder").GetComponent<SpriteRenderer>().sprite = _data.sprite;
		//3 frames to let the gameObject to initialize all components
		yield return null; yield return null; yield return null;
		Set();
	}

	public void SetSpriteLocalPosition(Vector3 spriteLocalPosition) {
		transform.Find("SpriteHolder").transform.localPosition = _locaPos = spriteLocalPosition;
	}

	public void SetLocalRotation(Vector3 collectableLocalRotation) {
		gameObject.transform.Rotate(collectableLocalRotation);
	}

	public string CollectableType { get { return _data.entityName; } }

}
