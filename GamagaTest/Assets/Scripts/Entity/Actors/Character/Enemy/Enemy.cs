﻿using UnityEngine;

public class Enemy : Character {

	[SerializeField]
	protected int _damagePoints = 1;

	[SerializeField]
	[Tooltip("At what distance on front the player should appear when Activate() is called.")]
	protected float _spawnDistance = 10f;

	// Use this for initialization
	public override void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		switch (_state) {
			case "idle":

				break;
			case "attacking":
				//If the collectable is out side the camera
				if (!(_data as EnemyData).estationary) {
					_avatar.transform.parent.Rotate(new Vector3(_xAxisRotationSpeed * _AxisRotationSmooth, 0, 0));
					if (Vector3.Distance(transform.position, _goal) > 0.1f) {
						transform.position = Vector3.MoveTowards(transform.position, _goal, _horizontalSpeed * Time.deltaTime);
					}
					if ((GameObject.FindGameObjectWithTag("Player").transform.position.x - 100f) >= transform.position.x) {
						Deactivate();
					}
				}
				break;
			case "passive":

				break;
		}
	}

	public override void Set() {
		_damagePoints = (_data != null) ? (_data as EnemyData).damagePoints : _damagePoints;
		_horizontalSpeed = (_data != null) ? _data.horizontalSpeed : _horizontalSpeed;
		_xAxisRotationSpeed = (_data != null) ? (_data as EnemyData).xAxisRotationSpeed : _xAxisRotationSpeed;
		transform.position = Vector3.zero;
		if (_avatar != null) {
			_avatar.transform.parent.rotation = new Quaternion();
		}
		_state = "idle";
	}

	public override void Activate() {
		transform.position = GameObject.FindGameObjectWithTag("Player").transform.position + (new Vector3(_spawnDistance, 0, 0));
		_avatar.transform.localPosition = _locaPos;
		_avatar.transform.parent.Rotate(Random.Range(0, 360f), 0, 0);
		_xAxisRotationSpeed = _data.xAxisRotationSpeed * ((Random.Range(0, 2) == 1) ? 1 : -1); //Randomizing the x axis rotation positive or negative, 50 - 50 chance
		_goal = Vector3.zero;
		_state = "attacking";
	}

	public override void Deactivate() {
		_state = "passive";
		_avatar.transform.localPosition = Vector3.zero;
		_avatar.transform.parent.rotation = new Quaternion();
	}

	public int damage { get { return (_data as EnemyData).damagePoints; } }

}
