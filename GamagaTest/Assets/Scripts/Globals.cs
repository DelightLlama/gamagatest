using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Globals {

    private static Menus _currentMenu = Menus.MainMenu;

    private static AsyncOperation _async;
    private static bool _loading = false;

    private static string _nextScene;

    public static SaveManager save;

    public static int originalScreenWidth;
    public static int originalScreenHeight;
    public static int originalScreenSize;
    public static float originalScreenAspect;

    public static int screenWidth = 1920;
    public static int screenHeight = 1080;
    public static int screenSize = screenHeight / 2;
    public static float maxZoomFactor;
    
    public static float frameTime;
    public const float frameTimeLimit = 30;

    private static AudioSource _audioSource;

    public static Color defaultCameraBackgroundColor = new Color(186f / 255f, 218f / 255f, 181f / 255f, 1f);

    public static void Pause () {
        Time.timeScale = 0;
        AudioListener.pause = true;
    }

    public static void Resume () {
        Time.timeScale = 1;
        AudioListener.pause = false;
    }

    public static void LoadMenu (Menus menu) {
        _currentMenu = menu;
        LoadScene(Scenes.Menu);
    }

    public static void LoadGame () {
        LoadScene(Scenes.Game);
    }

    public static void LoadScene (string scene) {
        _nextScene = scene;
        if (Time.timeScale == 0) {
            Time.timeScale = 1;
        }
        Utils.StopLiveAudios();
        if (AudioListener.pause) {
            AudioListener.pause = false;
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(_nextScene);
    }

    //Corutina que utiliza Loading Screen
    public static IEnumerator LoadSceneAsync () {
        _async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(_nextScene, UnityEngine.SceneManagement.LoadSceneMode.Single);
        _loading = true;
        //_async.allowSceneActivation = false;
        while(!_async.isDone) {
            yield return null;
        }
        _async = null;
        _loading = false;
    }

    public static float PlayClip(AudioClip clip, bool stopAudios = false, float volume = 1f) {
        float l = clip.length;

        if (stopAudios) {
            Utils.StopLiveAudios();
        }

        if (_audioSource != null) {
            _audioSource.spatialBlend = 0;
            _audioSource.PlayOneShot(clip, volume);
        } else {
            Vector3 pos = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
            AudioSource.PlayClipAtPoint(clip, pos, volume);
        }

        return l;
    }

    public static float loadProgress {
        get {
            if (_async != null) {
                return _async.progress;
            } else {
                return 0;
            }
        }
    }

    public static bool isLoadingScene {
        get {
            return _loading;
        }
    }

    public static Menus currentMenu {
        get {
            return _currentMenu;
        }
    }

    public static AudioSource audioSource {
        get {
            return _audioSource;
        }
        set {
            _audioSource = value;
            if (_audioSource != null) {
                _audioSource.spatialBlend = 0;
                _audioSource.volume = 1f;
            }
        }
    }

    public enum Menus {
        MainMenu,
    }

    public enum GAME_STATES {
        INITIALIZING,
        OVER,
        PAUSE,
        GAME,
    }

    public enum DIFFICULTY {
        NONE,
        LOW,
        MEDIUM,
        HIGH
    }

}
