﻿using System.Collections;
using UnityEngine;

public class GTMainScene : GTSceneBase {

    //TODO: This script should check for a save file, create if doesn't exist, load if exist, check for curruptions and create a new save file if there are

    // Use this for initialization
    public override void Start() {
        Globals.originalScreenWidth = Screen.width;
        Globals.originalScreenHeight = Screen.height;
        Globals.originalScreenSize = Globals.originalScreenHeight / 2;
        Globals.originalScreenAspect = (float)Globals.originalScreenWidth / Globals.originalScreenHeight;

        //gameObject.AddComponent<AspectUtility>();

        Globals.save = SaveManager.Instance;

        string[] saveFilesNames = Globals.save.GetSaveFilesNames();
        if (saveFilesNames != null) {
            if (saveFilesNames.Length <= 0) {
                Globals.save.CreateNewSaveFile("guest");
            } else {
                if (Globals.save.LoadData(saveFilesNames[0])) {
                    //Globals.save.PrintStars();
                }
            }
        } else {
            Globals.save.CreateNewSaveFile("guest");
        }

        StartCoroutine(AnimateSplash());
    }

    // Update is called once per frame
    public override void Update() {

    }

    private IEnumerator AnimateSplash() {
        yield return StartCoroutine(GameObject.Find("Splash").GetComponent<Splash>().Animate());

        Globals.LoadGame();
    }

}
