﻿using UnityEngine;

[CreateAssetMenu(fileName = "CollectableData", menuName = "Items/Collectable", order = 61)]
public class CollectableData : ItemData {

    [SerializeField]
    private int _points;

    public int points {
        get {
            return _points;
        }
    }

}
