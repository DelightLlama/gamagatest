﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public sealed class SaveManager {

    private static SaveManager _instance = null;

    private string savePath;

    private Save save;

    private SaveManager() {
        
    }

    public static SaveManager Instance {
        get {
            if (_instance == null) {
                _instance = new SaveManager();
            }
            return _instance;
        }
    }

    public bool LoadData(string userName) {
        this.userName = userName;
        savePath = Application.persistentDataPath + "/" + this.userName + ".save";
        if (File.Exists(savePath)) {
            var binaryFormatter = new BinaryFormatter();
            using (var fileStream = File.Open(savePath, FileMode.Open)) {
                save = (Save)binaryFormatter.Deserialize(fileStream);
            }
            return true;
        } else {
            return false;
        }
    }

    public bool CreateNewSaveFile(string userName) {
        this.userName = userName;
        savePath = Application.persistentDataPath + "/" + this.userName + ".save";
        if (!File.Exists(savePath)) {
            save = new Save();
            save.NewSave();
            SaveData();
            return true;
        } else {
            return false;
        }
    }

    public void DeleteSaveFile(string fileName) {
        string filePath = Application.persistentDataPath + "/" + fileName + ".save";
        if (File.Exists(filePath)) {
            File.Delete(filePath);
        }
    }

    public void SaveScore(int stars) {
        int currentStars = GetScore();
        if (currentStars < stars) {
            save.score["total"] = currentStars + (stars - currentStars);
            SaveData();
        }
    }

    public void SaveData() {
        var binaryFormatter = new BinaryFormatter();
        using (var fileStream = File.Create(savePath)) {
            binaryFormatter.Serialize(fileStream, save);
        }
    }

    public int GetScore() {
        return save.score["total"];
    }

    public void PrintScore() {
        save.PrintScore();
    }

    public string[] GetSaveFilesNames() {
        DirectoryInfo d = new DirectoryInfo(Application.persistentDataPath);
        FileInfo[] files = d.GetFiles(@"*.save");
        if (files != null) {
            if (files.Length > 0) {
                string[] saveFilesNames = new string[files.Length];
                for (int i = 0; i < files.Length; i++) {
                    saveFilesNames[i] = files[i].Name.Split('.')[0];
                }
                return saveFilesNames;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public string userName { get; private set; }

}
