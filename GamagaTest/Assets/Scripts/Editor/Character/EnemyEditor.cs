﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Enemy))]
public class EnemyEditor : Editor {

	private void OnEnable() {

	}

	public override void OnInspectorGUI() {
		Enemy script = (Enemy)target;
		DrawDefaultInspector();
		if (GUILayout.Button("Set Default Data values")) {
			if (script != null) {
				script.Set();
			}
		}
	}

}
