﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarActor : Actor {

	[SerializeField]
	protected AvatarData _data;

	protected GameObject _avatar;

	protected Vector3 _locaPos;

	protected Vector3 _goal;

	// Use this for initialization
	public virtual void Start () {
		StartCoroutine(SetReady());
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
	}

	public virtual void Set() {

	}

	protected virtual IEnumerator SetReady() {
		_avatar = Instantiate(_data.gameObject, transform.Find("AvatarHolder"));
		//3 frames to let the instance to initialize all components
		yield return null; yield return null; yield return null;
		_avatar.name = _data.gameObject.name;

		Set();
	}

	public void SetAvatarLocalPosition(Vector3 avatarLocalPosition) {
		_avatar.transform.localPosition = _locaPos = avatarLocalPosition;
	}

	public void SetAvatarLocalRotation(Vector3 modelLocalRotation) {
		_avatar.transform.parent.Rotate(modelLocalRotation);
	}

	public GameObject avatar { get { return _avatar; } }

	public string type { get { return _data.entityName; } }

}
