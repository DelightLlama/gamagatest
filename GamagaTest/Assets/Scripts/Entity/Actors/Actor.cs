﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : Entity {

	protected string _state;

	// Update is called once per frame
	public virtual void Update () {
		
	}

	public virtual void Activate() {
		
	}

	public virtual void Deactivate() {
		
	}

	public string state { get { return _state; } }

}
