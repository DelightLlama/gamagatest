﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Collectable))]
public class CollectableEditor : Editor {

	private void OnEnable() {

	}

	public override void OnInspectorGUI() {
		Collectable script = (Collectable)target;
		DrawDefaultInspector();
		if (GUILayout.Button("Set Default Data values")) {
			if (script != null) {
				script.Set();
			}
		}
	}

}
