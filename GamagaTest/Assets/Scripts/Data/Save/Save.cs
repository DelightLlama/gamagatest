﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save {

    public int ver;

    public Dictionary<string, int> score;

    public Save() {

    }

    public void NewSave() {
        ver = 0;

        score = new Dictionary<string, int>();

        score.Add("Ver", ver);
        score.Add("total", 0);
    }

    public void RebuildSave() {
        if (score["Ver"] < ver) {

        }
    }
    
    public void PrintScore() {
        Debug.Log("Version:" + score["Ver"]);
        Debug.Log("Current score:");
        List<string> keys = new List<string>(score.Keys);
        foreach (string key in keys) {
			if (!key.Equals("Ver")) {
				Debug.Log(key + ": " + score[key]);
			}
        }
    }

}
