﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderTerrain : Entity {

	private float _xAxisRotation = 0;

	// Use this for initialization
	void Start () {
		Transform _middlePart = transform.Find("Middle");
		_middlePart.position = Utils.MidPoint(transform.Find("Start").position, transform.Find("Finish").position);
		float _percent = (Vector3.Distance(transform.Find("Start").position, transform.Find("Finish").position) / _middlePart.GetComponent<MeshRenderer>().bounds.size.y) * 0.5f;
		_middlePart.localScale = new Vector3(1f, _percent, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(_xAxisRotation, 0, 0));
	}

	public void AddXAxisRotation (float angles) {
		_xAxisRotation = angles;
	}

}
