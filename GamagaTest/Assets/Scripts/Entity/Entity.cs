﻿using UnityEngine;

public class Entity : MonoBehaviour {

	[SerializeField, Range(0, float.MaxValue)]
	protected float _horizontalSpeed = 0;

	[SerializeField]
	protected float _xAxisRotationSpeed = 0;

	[SerializeField, Range(0.25f, 1f)]
	protected float _AxisRotationSmooth;

}
