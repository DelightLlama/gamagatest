﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    private void FixedUpdate() {

    }

    public IEnumerator Animate() {
        int i, len = gameObject.transform.childCount;
		int[] indexes = new int[len];
        float delay = 0.2f;

		for (i = 0; i < len; i++) {
			indexes[i] = i;
		}
		Utils.ShuffleArray(ref indexes);

		for (i = 0; i < (len - 1); i++) {
            StartCoroutine(RotateLetter(indexes[i], delay));
			delay += 0.02f;
		}
		delay += 0.02f;
		StartCoroutine(RotateLetter(indexes[(len - 1)], delay));

		yield return new WaitForSeconds(3f);
    }

    private IEnumerator RotateLetter(int letterIndex, float delay) {
        yield return new WaitForSeconds(delay);
        Transform letter = gameObject.transform.GetChild(letterIndex);
        Vector3 dest = new Vector3(0, 0, 0);
        do {
            letter.eulerAngles = new Vector3(Mathf.LerpAngle(letter.eulerAngles.x, dest.x, Time.deltaTime),
                                             Mathf.LerpAngle(letter.eulerAngles.y, dest.y, Time.deltaTime),
                                             Mathf.LerpAngle(letter.eulerAngles.z, dest.z, Time.deltaTime));
            yield return null;
        } while (Vector3.Distance(letter.rotation.eulerAngles, dest) > 0.1f);
        letter.eulerAngles = dest;
    }

}
