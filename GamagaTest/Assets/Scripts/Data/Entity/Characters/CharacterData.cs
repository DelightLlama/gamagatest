﻿using UnityEngine;

public class CharacterData : EntityData {

	[SerializeField]
	private float _terrainPadding;

	public float terrainPadding { get { return _terrainPadding; } }

}
